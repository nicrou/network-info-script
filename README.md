A simple Powershell script that enumerates the machines of the network and displays information about them if possible.

#### Option #1:

Execute without any arguments but the subnet and it will try to collect information from 1 to 254.

`.\networkInfo.ps1 192.168.2.0`

> Sample output scr-1.png
>

#### Option #2:

Execute with the subnet and a range, i.e. the following will scan from 192.168.2.3 to 192.168.2.7.

`.\networkInfo.ps1 192.168.2.0 3 7`

> Sample output scr-2.png
>

#### Option #3:

Execute with the subnet and just where to start, i.e. the following will scan from 192.168.2.7 to 192.168.2.254.

`.\networkInfo.ps1 192.168.2.0 7`

> Sample output scr-3.png