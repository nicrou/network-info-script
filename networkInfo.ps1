Param (
    [Parameter(position=0, mandatory=$true)] $subnet,
    [Parameter(position=1, mandatory=$false)] $rangeFrom = 1,
    [Parameter(position=2, mandatory=$false)] $rangeTo = 254
)

if($rangeFrom -lt 1 -or $rangeFrom -gt 254) { throw 'Invalid IP address.' }
if($rangeTo -lt 1 -or $rangeTo -gt 254 -or $rangeTo -lt $rangeFrom) { throw 'Invalid IP address.' }

Write-Host $rangeFrom
Write-Host $rangeTo

$network = [string]::join(".", $subnet.split(".")[0-4], $subnet.split(".")[0-3], $subnet.split(".")[0-2])
For($h=$rangeFrom; $h -lt $rangeTo; $h++) {
    $addr = [string]::join(".", ($network, $h))
    if(!(Test-Connection -Cn $addr -BufferSize 16 -Count 1 -ea 0 -quiet)) {
       Write-Host "Cannot reach $addr offline." -f red
    } else {
        Try{
           $nwINFO = Get-WmiObject -ComputerName $addr Win32_NetworkAdapterConfiguration -ErrorAction Stop | Where-Object { $_.IPAddress -ne $null } 
           $nwServerName = $nwINFO.DNSHostName[0]
           $nwDescrip = $nwINFO.Description[0]
           $nwIPADDR = $nwINFO.IPAddress[0]
           $nwSUBNET = $nwINFO.IpSubnet[0]
           $nwGateWay = $nwINFO.DefaultIPGateway
           $nwMacADD = $nwINFO.MACAddress[0]
           $nwDNS = $nwINFO.DNSServerSearchOrder
           Write-Host "Server/Computer Name  :   $nwServerName `nNetwork Card          :   $nwDescrip `nIPAdress              :   $nwIPADDR `nSubnet Mask           :   $nwSUBNET `nGateway               : $nwGateWay `nMAC Address           :   $nwMacADD `nDNS                   : $nwDNS `n" 
        }
        catch [Exception]
        {
           "Could not get information from $addr, it is alive but most likely not a Windows device. Skipping to next."   
        }
    }
}